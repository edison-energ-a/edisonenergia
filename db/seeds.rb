# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(
  email: 'juanfletes@gmail.com',
  encrypted_password: '$2a$12$xXfp2yok58.hEyqWJTZ2e.mqud1MGY88Q4PiBoILqyaBi4s365.WG'
)
